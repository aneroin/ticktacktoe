﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : Controller {

    //Upper level controller
    GameController gameController;

    //Board data
    Board board;
    //Board view
    BoardView view;
    //Render camera
    Camera cam;

    //Token, indicate which player has turn. A goes first
    bool PassToken;

    //Players
    Player A;
    Player B;

    //Render rect anchors for the board
    public Vector2 bl;
    public Vector2 tr;

    /// <summary>
    /// DataEvent callback. Each time data event is generated, this delegate is called on each binded model.
    /// </summary>
    /// <param name="boardData"></param>
    public void BoardChanged(DataEvent<Board.CellType[,]> boardData){
        view.SetData(boardData.Data());
        if (CheckWinningCondition(boardData.Data()))
        {
            if (PassToken)
                gameController.PlayerBWin();
            else
                gameController.PlayerAWin();

            return;
        } else if (CheckDrawCondition(boardData.Data()))
        {
            gameController.Draw();
            return;
        }
        PassToken = !PassToken;
        if (PassToken)
            if (B is BehaviourNormalAI) StartCoroutine(Think(() => { B.MakeTurn(boardData.Data(), MakeTurn); }));
            else B.MakeTurn(boardData.Data(), MakeTurn);
        else
            if (A is BehaviourNormalAI) StartCoroutine(Think(() => { A.MakeTurn(boardData.Data(), MakeTurn); }));
            else A.MakeTurn(boardData.Data(), MakeTurn);
    }

    /// <summary>
    /// Thinking Emulator
    /// </summary>
    /// <param name="t"></param>
    /// <param name="move"></param>
    /// <returns></returns>
    IEnumerator Think(System.Action move)
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 3f));
        move();
        yield return true;
    }

    /// <summary>
    /// Set proper cell based on Player's turn
    /// </summary>
    /// <param name="data"></param>
    public void MakeTurn(DataEvent<int[]> data)
    {
        //TODO check Data.Lenght
        Debug.Log("BoardController Turn of " + (PassToken ? "PlayerB":"PlayerA"));
        int i, k;
        i = data.Data()[0];
        k = data.Data()[1];
        board.SetCell(i, k, PassToken ? Board.CellType.o : Board.CellType.x);
    }

    public void OnClick(View view)
    {
        if (view is PlatformView)
        {
            PlatformView platform = (PlatformView)view;
            if (A is RealPlayer)
                (A as RealPlayer).ClickListener(platform.getX(), platform.getY());
            if (B is RealPlayer)
                (B as RealPlayer).ClickListener(platform.getX(), platform.getY());
        }
        if (view is MenuPlatformView)
        {
            MenuPlatformView menu = (MenuPlatformView)view;
            gameController.OnState(menu.getState());
        }
    }


	void Start () {

        gameController = FindObjectOfType<GameController>();

        cam = Camera.main;

        board = new Board();
        board.Bind(BoardChanged);
        view = gameObject.AddComponent<BoardView>();

        bl = V3toV2(cam.ScreenToWorldPoint(new Vector3(32f, 32f, cam.transform.position.y)));
        tr = V3toV2(cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth - 32f, cam.pixelHeight - 32f, cam.transform.position.y)));

        view.Bind(this.OnClick);
        view.SetRenderRect(bl, tr);
        cam.transform.position = view.transform.position + new Vector3(0f, 10f, 0f);

        StartCoroutine(Slowpoke());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameController.Menu();
        }

    }

    IEnumerator Slowpoke()
    {
        yield return new WaitForSeconds(3f);
        if (PassToken)
            if (B is BehaviourNormalAI) StartCoroutine(Think(() => { B.MakeTurn(PossibleBoardStates.StateClear, MakeTurn); }));
            else B.MakeTurn(PossibleBoardStates.StateClear, MakeTurn);
        else
            if (A is BehaviourNormalAI) StartCoroutine(Think(() => { A.MakeTurn(PossibleBoardStates.StateClear, MakeTurn); }));
        else A.MakeTurn(PossibleBoardStates.StateClear, MakeTurn);
        yield return true;
    }

    /// <summary>
    /// Setup players for the match
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    public void SetPlayers(Player A, Player B, bool PassToken)
    {
        this.PassToken = PassToken;

        this.A = A;
        this.B = B;
    }

    /// <summary>
    /// Unbind all listeners from board to make it unresponsive
    /// </summary>
    public void DisconnectBoard()
    {
        view.UnBind(this.OnClick);
        board.Unbind(this.BoardChanged);
    }

    /// <summary>
    /// Destroy board view
    /// </summary>
    public void DestroyBoard()
    {
        view.DestroyView();
        Destroy(view);
    }

    /// <summary>
    /// Just a little utility to make Vector2 from Vector3.x and Vector3.z components.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    //TODO: REMOVE!!!
    Vector2 V3toV2(Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }


    /// <summary>
    /// Check if draw
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    bool CheckDrawCondition(Board.CellType[,] board)
    {
        foreach (Board.CellType cell in board)
        {
            if (cell == Board.CellType.empty) return false;
        }

        return true;
    }

    /// <summary>
    /// Check if someone win
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    bool CheckWinningCondition(Board.CellType[,] board)
    {
        for (int k = 0; k < 3; k++)
        {
            if (CheckHorizontalLine(board, k)) return true;
            if (CheckVerticalLine(board, k)) return true;
        }

        if (CheckDiagonalLines(board)) return true;

        return false;
    }

    /// <summary>
    /// Check if 3 in a row horizontally
    /// </summary>
    /// <param name="board"></param>
    /// <param name="k"></param>
    /// <returns></returns>
    bool CheckHorizontalLine(Board.CellType[,] board, int k)
    {
        return board[0, k] == board[1, k] && board[1, k] == board[2, k] && board[1,k]!=Board.CellType.empty;
    }

    /// <summary>
    /// Check if 3 in a row vertically
    /// </summary>
    /// <param name="board"></param>
    /// <param name="k"></param>
    /// <returns></returns>
    bool CheckVerticalLine(Board.CellType[,] board, int k)
    {
        return board[k, 0] == board[k, 1] && board[k, 1] == board[k, 2] && board[k, 1] != Board.CellType.empty;
    }

    /// <summary>
    /// Check if 3 in a row diagonally
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    bool CheckDiagonalLines(Board.CellType[,] board)
    {
        return board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2] && board[1, 1] != Board.CellType.empty 
            || board[0, 2] == board[1, 1] && board[1, 1] == board[2, 0] && board[1, 1] != Board.CellType.empty;
    }
}
