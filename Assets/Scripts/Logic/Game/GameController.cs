﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : Controller {

    bool DrawToken;

    //AI Confidence rate
    [Range(0.0f, 1.0f)]
    public float AI_Confidence;

    //Initial State. For debug purposes.
    public Game.GameState initialState;

    //For behaviour
    public Game.GameState current_state;
    public Game.GameState previous_state;

    //Game states
    Dictionary<Game.GameState, System.Action> gameFlow;

    //Game data
    public Game game;
    //Board controller's gameobject
    GameObject boardHolder;
    //Board controller
    BoardController boardController;

    public void Start()
    {
        //================SINGLE INSTANCE ONLY!====================
        GameController gc = FindObjectOfType<GameController>();
        if (gc != null && gc != this)
        {
            Destroy(gameObject);
            return;
        }
        else
            DontDestroyOnLoad(this);
        //=========================================================
        
        gameFlow = new Dictionary<Game.GameState, System.Action>() {
            {
                Game.GameState.menu,
                () =>
                {
                    if (previous_state==Game.GameState.play || previous_state==Game.GameState.playerA || previous_state==Game.GameState.playerB || previous_state==Game.GameState.draw){
                        SceneManager.LoadScene("MenuScene");
                    } else
                    {
                        Debug.LogWarning("Menu View");
                        GameObject menuHolder = new GameObject("MenuHolder");
                        MenuView menu = menuHolder.AddComponent<MenuView>();
                        Camera cam = Camera.main;
                        Vector3 bl = cam.ScreenToWorldPoint(new Vector3(32f, 32f, cam.farClipPlane / 3f));
                        Vector3 tr = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth - 32f, cam.pixelHeight - 32f, cam.farClipPlane / 3f));
                        menu.SetRenderRect(bl, tr);
                        menu.Bind(OnClick);
                        cam.transform.position = menu.transform.position + new Vector3(0f, 1f, -cam.farClipPlane / 4f);
                    }
                }
            },
            {
                Game.GameState.introduction,
                () =>
                {
                    
                }
            },
            {
                Game.GameState.play,
                () =>
                {
                    if (previous_state==Game.GameState.menu)
                            FindObjectOfType<MenuView>().DestroyView();

                    if (previous_state != Game.GameState.play){
                        SceneManager.LoadScene("DebugScene");
                    } else
                    {
                        boardHolder = GameObject.FindGameObjectWithTag("BoardController");
                        boardController = boardHolder.AddComponent<BoardController>();
                        boardController.SetPlayers(new RealPlayer(), new BehaviourNormalAI(AI_Confidence), false);
                    }
                }
            },
            {
                Game.GameState.score,
                () =>
                {
                    Debug.LogWarning("Show Score");
                }
            },
            {
                Game.GameState.playerA,
                () =>
                {
                    Debug.LogWarning("player A");
                    boardController.DisconnectBoard();

                    StartCoroutine(runWithDelay(2f, () => {
                        Debug.LogWarning("DestroyBoard");
                        boardController.DestroyBoard();
                        Destroy(boardController);
                    }));

                    //TODO: Add Score View!!!

                    StartCoroutine(runWithDelay(2.5f, () => {
                        Debug.LogWarning("InitBoard");
                        boardController = boardHolder.AddComponent<BoardController>();
                        boardController.SetPlayers(new RealPlayer(), new BehaviourNormalAI(AI_Confidence), false);
                    }));
                }
            },
            {
                Game.GameState.playerB,
                () =>
                {
                    Debug.LogWarning("player B");
                    boardController.DisconnectBoard();

                    StartCoroutine(runWithDelay(2f, () => {
                        Debug.LogWarning("DestroyBoard");
                        boardController.DestroyBoard();
                        Destroy(boardController);
                    }));

                    //TODO: Add Score View!!!

                    StartCoroutine(runWithDelay(2.5f, () => {
                        Debug.LogWarning("InitBoard");
                        boardController = boardHolder.AddComponent<BoardController>();
                        boardController.SetPlayers(new RealPlayer(), new BehaviourNormalAI(AI_Confidence), true);
                    }));
                }
            },
            {
                Game.GameState.draw,
                () =>
                {
                    if (previous_state==Game.GameState.playerA||previous_state==Game.GameState.play)
                        DrawToken = true;
                    else
                        DrawToken = !DrawToken;
                    Debug.LogWarning("draw");
                    boardController.DisconnectBoard();

                    StartCoroutine(runWithDelay(2f, () => {
                        Debug.LogWarning("DestroyBoard");
                        boardController.DestroyBoard();
                        Destroy(boardController);
                    }));

                    //TODO: Add Score View!!!

                    StartCoroutine(runWithDelay(2.5f, () => {
                        Debug.LogWarning("InitBoard");
                        boardController = boardHolder.AddComponent<BoardController>();
                        boardController.SetPlayers(new RealPlayer(), new BehaviourNormalAI(AI_Confidence), DrawToken);
                    }));

                    
                }
            }
        };

        game = new Game();
        game.Bind(GameScoreChanged);
        game.Bind(GameStateChanged);

        current_state = initialState;
        game.state = initialState;

        SceneManager.sceneLoaded += OnSceneSwitch;
    }

    /// <summary>
    /// React on scene load;
    /// </summary>
    /// <param name="aScene"></param>
    /// <param name="aMode"></param>
    private void OnSceneSwitch(Scene scene, LoadSceneMode mode)
    {
            if (scene == SceneManager.GetSceneByName("DebugScene"))
            {
                boardHolder = GameObject.FindGameObjectWithTag("BoardController");
                boardController = boardHolder.AddComponent<BoardController>();
                boardController.SetPlayers(new RealPlayer(), new BehaviourNormalAI(AI_Confidence), false);
            }

            else if (scene == SceneManager.GetSceneByName("MenuScene"))
            {
                Debug.LogWarning("Menu View");
                GameObject menuHolder = new GameObject("MenuHolder");
                MenuView menu = menuHolder.AddComponent<MenuView>();
                Camera cam = Camera.main;
                Vector3 bl = cam.ScreenToWorldPoint(new Vector3(32f, 32f, cam.farClipPlane / 3f));
                Vector3 tr = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth - 32f, cam.pixelHeight - 32f, cam.farClipPlane / 3f));
                menu.SetRenderRect(bl, tr);
                menu.Bind(OnClick);
                cam.transform.position = menu.transform.position + new Vector3(0f, 1f, -cam.farClipPlane / 4f);
            }
    }

    /// <summary>
    /// View onClick callback
    /// </summary>
    /// <param name="view"></param>
    public void OnClick(View view)
    {
        if (view is MenuPlatformView)
        {
            MenuPlatformView menu = (MenuPlatformView)view;
            switch (menu.payload)
            {
                case "easy":
                    {
                        AI_Confidence = 0.35f;
                        break;
                    }
                case "normal":
                    {
                        AI_Confidence = 0.85f;
                        break;
                    }
                case "unfair":
                    {
                        AI_Confidence = 1f;
                        break;
                    }
                default:
                    {
                        AI_Confidence = 0.85f;
                        break;
                    }
            }
            OnState(menu.getState());
        }
    }

    /// <summary>
    /// Pass game state
    /// </summary>
    /// <param name="state"></param>
    public void OnState(Game.GameState state)
    {
        game.state = state;
    }

    /// <summary>
    /// Callback on game state change
    /// </summary>
    /// <param name="gameState"></param>
    public void GameStateChanged(DataEvent<Game.GameState> gameState)
    {
        previous_state = current_state;
        current_state = gameState.Data();
        gameFlow[gameState.Data()]();
    }

    /// <summary>
    /// Callback on game score change
    /// </summary>
    /// <param name="gameData">game scores, 0 = player A, 1 = player B, 2 = draws</param>
    public void GameScoreChanged(DataEvent<int[]> scoreData)
    {
        int[] scores = scoreData.Data();

        Debug.LogWarning(string.Format("PlayerA {0} - PlayerB {1} || ({2} Draws)", scores[0], scores[1], scores[2]));
    }

    /// <summary>
    /// Indicate PlayerA as a winner
    /// </summary>
    public void PlayerAWin()
    {
        game.PlayerAWin();
    }

    /// <summary>
    /// Indicate PlayerB as a winner
    /// </summary>
    public void PlayerBWin()
    {
        game.PlayerBWin();
    }

    /// <summary>
    /// Indicate game draw
    /// </summary>
    public void Draw()
    {
        game.Draw();
    }

    /// <summary>
    /// Indicate menu
    /// </summary>
    public void Menu()
    {
        game.Menu();
    }

    /// <summary>
    /// Runs some action with delay specified
    /// </summary>
    /// <param name="t">delay</param>
    /// <param name="action">action to run</param>
    /// <returns></returns>
    IEnumerator runWithDelay(float t, System.Action action)
    {
        yield return new WaitForSeconds(t);
        action();
        yield return true;
    }
}
