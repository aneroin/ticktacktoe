﻿public interface State {
    void Execute();
}
