﻿using System.Collections.Generic;

/// <summary>
/// Transition to the next state
/// </summary>
public class Transition {
    //Possible states
    protected List<State> states;
    //Corresponding weights
    protected List<float> weights;

    /// <summary>
    /// Singlestate transition
    /// </summary>
    /// <param name="state"></param>
    public Transition(State state)
    {
        states = new List<State>(0);
        states.Add(state);
    }

    /// <summary>
    /// Manually weighted multistate transition
    /// </summary>
    /// <param name="states"></param>
    /// <param name="weights"></param>
    public Transition(State[] states, float[] weights)
    {
        if (states.Length != weights.Length)
            throw new UnityEngine.UnityException("each state should have weight");
        this.states = new List<State>();
        this.weights = new List<float>();
        this.states.AddRange(states);
        this.weights.AddRange(weights);
    }

    /// <summary>
    /// Auto equally weighted multistate transition
    /// </summary>
    /// <param name="states"></param>
    public Transition(State[] states)
    {
        this.states = new List<State>();
        this.weights = new List<float>();
        this.states.AddRange(states);
        for (int i = 0; i < this.states.Count; i++)
            this.weights.Add(1f);
    }

    /// <summary>
    /// Transfer to the next State from the list of possible states
    /// </summary>
    /// <returns></returns>
    public State Transfer()
    {
        if (states.Count == 1) // single state transfer
            return states[0];
        else { // multi state transfer
            float totalWeights = 0f;
            for (int i = 0; i < weights.Count; i++)
                totalWeights += weights[i];
            float pointer = UnityEngine.Random.Range(0f, totalWeights);
            for (int i = 0; i < weights.Count; i++)
            {
                pointer -= weights[i];
                if (pointer <= 0f)
                    return states[i];
            }
            throw new UnityEngine.UnityException("no proper state were reached");
        }
    }

}
