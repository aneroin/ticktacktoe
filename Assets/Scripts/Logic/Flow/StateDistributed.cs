﻿public interface StateDistributed : State {
    void Enter();
    void Exit();
}
