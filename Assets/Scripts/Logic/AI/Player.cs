﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Player {
    void MakeTurn(Board.CellType[,] boardState, Controller.EventCallback<int[]> TurnCallback);
}
