﻿using System.Collections;
using System.Collections.Generic;

public class IdiotAI : Player {

    Dictionary<Board.CellType[,], Transition> transitions;

    public IdiotAI()
    {
        transitions = new Dictionary<Board.CellType[,], Transition>(new BoardStateComparer());

        transitions.Add(PossibleBoardStates.State0100, new Transition(new State[] { new StatePlaceC(), new StatePlaceT(), new StatePlaceB(), new StatePlaceL(), new StatePlaceR() }, new float[] { 2f, 0.25f, 0.25f, 0.25f, 0.25f }));
        transitions.Add(PossibleBoardStates.State0101, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0102, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0103, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0104, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0105, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0106, new Transition(new StatePlaceC()));
        transitions.Add(PossibleBoardStates.State0107, new Transition(new StatePlaceC()));

        transitions.Add(PossibleBoardStates.State0200, new Transition(new StatePlaceBR()));
        transitions.Add(PossibleBoardStates.State0201, new Transition(new StatePlaceBR()));

        transitions.Add(PossibleBoardStates.State0202, new Transition(new StatePlaceTR()));
        transitions.Add(PossibleBoardStates.State0203, new Transition(new StatePlaceTR()));

        transitions.Add(PossibleBoardStates.State0204, new Transition(new StatePlaceTL()));
        transitions.Add(PossibleBoardStates.State0205, new Transition(new StatePlaceTL()));

        transitions.Add(PossibleBoardStates.State0206, new Transition(new StatePlaceBL()));
        transitions.Add(PossibleBoardStates.State0207, new Transition(new StatePlaceBL()));

        transitions.Add(PossibleBoardStates.State0300, new Transition(new StatePlaceL()));
        transitions.Add(PossibleBoardStates.State0303, new Transition(new StatePlaceL()));

        transitions.Add(PossibleBoardStates.State0304, new Transition(new StatePlaceR()));
        transitions.Add(PossibleBoardStates.State0307, new Transition(new StatePlaceR()));

        transitions.Add(PossibleBoardStates.State0301, new Transition(new StatePlaceT()));
        transitions.Add(PossibleBoardStates.State0306, new Transition(new StatePlaceT()));

        transitions.Add(PossibleBoardStates.State0302, new Transition(new StatePlaceB()));
        transitions.Add(PossibleBoardStates.State0305, new Transition(new StatePlaceB()));

        transitions.Add(PossibleBoardStates.State0400, new Transition(new StatePlaceTR()));
        transitions.Add(PossibleBoardStates.State0405, new Transition(new StatePlaceTR()));

        transitions.Add(PossibleBoardStates.State0401, new Transition(new StatePlaceBL()));
        transitions.Add(PossibleBoardStates.State0404, new Transition(new StatePlaceBL()));

        transitions.Add(PossibleBoardStates.State0402, new Transition(new StatePlaceTL()));
        transitions.Add(PossibleBoardStates.State0407, new Transition(new StatePlaceTL()));

        transitions.Add(PossibleBoardStates.State0403, new Transition(new StatePlaceBR()));
        transitions.Add(PossibleBoardStates.State0406, new Transition(new StatePlaceBR()));


    }
    
    public void MakeTurn(Board.CellType[,] boardState, Controller.EventCallback<int[]> TurnCallback)
    {
        UnityEngine.Debug.Log("IdiotAI Make Turn");
        Transition nextTransition;
        State nextState;

        if (transitions.TryGetValue(boardState, out nextTransition))
        {
            nextState = nextTransition.Transfer();
            nextState.Execute();
        }
    }

}
