﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using ChainBranch = System.Collections.Generic.Dictionary<Board.CellType[,], System.Collections.Generic.KeyValuePair<State, ChainTransition>>;

public class ChainedIdiotAI : Player {

    ChainTransition globalState;
    ChainTransition state;

    Board.CellType[,] prevBoardState;
    
    public ChainedIdiotAI()
    {
        prevBoardState = PossibleBoardStates.StateClear;

        globalState = new ChainTransition(new ChainBranch()
        {
            {
                PossibleBoardStates.StateC, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceTL(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceT(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateBL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                    )
                                                )
                                            }
                                        }    
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateL, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                        }    
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }    
                                                    )
                                                )
                                            }
                                        }    
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch(){
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }

                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }    
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceT(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }

                                        }    
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTR(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateBL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }    
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            }
                        }    
                    )
                )
            }, {
                PossibleBoardStates.StateR, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }  
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }    
                                                    )
                                                )
                                            }
                                        }    
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceL(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateT,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateB,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }    
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceB(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateBL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceB(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateB,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }    
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceB(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateT,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateT, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }    
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceB(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateB, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },
                                            {
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            }
                        }    
                    )
                )
            }, {
                PossibleBoardStates.StateTL, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch(){
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }

                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceT(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateTR, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceT(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }

                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceB(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateBL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceB(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateB,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceT(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateBR, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceT(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTR(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceBL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateBL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceB(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateT,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceBL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateBL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceB(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTL(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTL,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceL(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTR(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateBL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateBL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBL(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            }
                        }
                    )
                )
            }, {
                PossibleBoardStates.StateBL, //COMPLETED
                new KeyValuePair<State, ChainTransition>(
                    new StatePlaceC(),
                    new ChainTransition(
                        new ChainBranch()
                        {
                            {
                                PossibleBoardStates.StateL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceT(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                            new ChainBranch()
                                            {
                                                {
                                                    PossibleBoardStates.StateT,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateB,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTR,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceTL(),
                                                        new ChainTransition()
                                                    )
                                                },{
                                                    PossibleBoardStates.StateTL,
                                                    new KeyValuePair<State, ChainTransition>(
                                                        new StatePlaceL(),
                                                        new ChainTransition(
                                                            new ChainBranch()
                                                            {
                                                                {
                                                                    PossibleBoardStates.StateT,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateB,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceTR(),
                                                                        new ChainTransition()
                                                                    )
                                                                },{
                                                                    PossibleBoardStates.StateTR,
                                                                    new KeyValuePair<State, ChainTransition>(
                                                                        new StatePlaceT(),
                                                                        new ChainTransition()
                                                                    )
                                                                }
                                                            }
                                                        )
                                                    )
                                                }
                                            }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateT,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceTL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceBR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceB(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateB,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceBR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTL,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceL(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceR(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateTR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceR(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateBR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateB,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceTL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateT,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateB,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceBR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateBR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceB(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            },
                            {
                                PossibleBoardStates.StateBR,
                                new KeyValuePair<State, ChainTransition>(
                                    new StatePlaceB(),
                                    new ChainTransition(
                                        new ChainBranch()
                                        {
                                            {
                                                PossibleBoardStates.StateL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTL,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateTR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateR,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceT(),
                                                    new ChainTransition()
                                                )
                                            },{
                                                PossibleBoardStates.StateT,
                                                new KeyValuePair<State, ChainTransition>(
                                                    new StatePlaceL(),
                                                    new ChainTransition(
                                                        new ChainBranch()
                                                        {
                                                            {
                                                                PossibleBoardStates.StateTL,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateTR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceR(),
                                                                    new ChainTransition()
                                                                )
                                                            },{
                                                                PossibleBoardStates.StateR,
                                                                new KeyValuePair<State, ChainTransition>(
                                                                    new StatePlaceTR(),
                                                                    new ChainTransition()
                                                                )
                                                            }
                                                        }
                                                    )
                                                )
                                            }
                                        }
                                    )
                                )
                            }
                        }
                    )
                )
            }
        });

        state = globalState;
    }

    public void MakeTurn(Board.CellType[,] boardState, Controller.EventCallback<int[]> TurnCallback)
    {
        Board.CellType[,] diff = PossibleBoardStates.Diff(prevBoardState, boardState);
        this.prevBoardState = (Board.CellType[,]) boardState.Clone();
        if (state == null)
            return;
        KeyValuePair<State,ChainTransition> next = state.Transfer(diff);
        state = next.Value;
        next.Key.Execute();
    }

}
