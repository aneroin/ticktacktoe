﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealPlayer : Player {

    Controller.EventCallback<int[]> turn;
    Board.CellType[,] board;

    /// <summary>
    /// Let player make a turn, pass it one-use turn callback
    /// </summary>
    /// <param name="boardState"></param>
    /// <param name="TurnCallback"></param>
    public void MakeTurn(Board.CellType[,] boardState, Controller.EventCallback<int[]> TurnCallback)
    {
        turn = TurnCallback;
        board = boardState;
    }

    /// <summary>
    /// Call player to make a turn, if it has the one-use turn callback
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void ClickListener(int x, int y)
    {
        if (board!=null && board[x, y] == Board.CellType.empty)
        {
            Controller.EventCallback<int[]> t = turn;
            if (turn != null)
            {
                turn = null;
                t(new DataEvent<int[]>(new int[] { x, y }));
            }
        }
    }
}
