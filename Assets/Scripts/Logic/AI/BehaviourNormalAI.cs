﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourNormalAI : Player {

    //Confidence of this AI. Higher is better.
    float confidence;

    /// <summary>
    /// Default AI with confidence 0.75f
    /// </summary>
    public BehaviourNormalAI()
    {
        this.confidence = 0.75f;
    }

    /// <summary>
    /// Construct AI with given confidence
    /// </summary>
    /// <param name="confidence">from 0f of total idiot to 1.0f of normal play</param>
    public BehaviourNormalAI(float confidence)
    {
        this.confidence = confidence;
    }

    /// <summary>
    /// Make a turn
    /// </summary>
    /// <param name="boardState">current board layout</param>
    /// <param name="TurnCallback">callback to make a turn</param>
    public void MakeTurn(Board.CellType[,] boardState, Controller.EventCallback<int[]> TurnCallback)
    {
        State next;
        if (Random.value <= confidence)
            if (SelfWinCond(boardState, out next))
            {
                next.Execute();
                return;
            }
        if (Random.value <= confidence)
            if (EnemyWinCond(boardState, out next))
            {
                next.Execute();
                return;
            }
        GodPlaysDice(boardState).Execute();
    }

    /// <summary>
    /// Check if enemy can win
    /// </summary>
    /// <param name="board">board layout</param>
    /// <param name="state">appropriate state action</param>
    /// <returns></returns>
    bool EnemyWinCond(Board.CellType[,] board, out State state)
    {
        int[] coords = new int[2];

        if (DetectVLine(board, Board.CellType.x, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }
        if (DetectHLine(board, Board.CellType.x, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }
        if (DetectDLine(board, Board.CellType.x, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }

        state = new StateTerminal();
        return false;
    }

    /// <summary>
    /// Check if self can win
    /// </summary>
    /// <param name="board">board layout</param>
    /// <param name="state">appropriate state action</param>
    /// <returns></returns>
    bool SelfWinCond(Board.CellType[,] board, out State state)
    {
        int[] coords = new int[2];

        if (DetectVLine(board, Board.CellType.o, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }
        if (DetectHLine(board, Board.CellType.o, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }
        if (DetectDLine(board, Board.CellType.o, out coords))
        {
            state = CoordsToState(coords);
            return true;
        }

        state = new StateTerminal();
        return false;
    }

    /// <summary>
    /// If nothing close to win, play dice
    /// </summary>
    /// <param name="board">board layout</param>
    /// <returns></returns>
    State GodPlaysDice(Board.CellType[,] board)
    {
        List<int[]> freeCells = new List<int[]>();
        for (int i = 0; i < 3; i++)
            for(int k = 0; k < 3; k++)
                if (board[i, k] == Board.CellType.empty)
                    freeCells.Add(new int[2] { i, k });
        return CoordsToState(freeCells[Random.Range(0, freeCells.Count)]);
    }

    /// <summary>
    /// Convert coordinates to state action
    /// </summary>
    /// <param name="coords"></param>
    /// <returns></returns>
    State CoordsToState(int[] coords)
    {
        switch (coords[0])
        {
            case 0:
                {
                    switch (coords[1])
                    {
                        case 0:
                            {
                                return new StatePlaceBL();
                                break;
                            }
                        case 1:
                            {
                                return new StatePlaceL();
                                break;
                            }
                        case 2:
                            {
                                return new StatePlaceTL();
                                break;
                            }
                        default:
                            return new StateTerminal();
                    }
                    break;
                }
            case 1:
                {
                    switch (coords[1])
                    {
                        case 0:
                            {
                                return new StatePlaceB();
                                break;
                            }
                        case 1:
                            {
                                return new StatePlaceC();
                                break;
                            }
                        case 2:
                            {
                                return new StatePlaceT();
                                break;
                            }
                        default:
                            return new StateTerminal();
                    }
                    break;
                }
            case 2:
                {
                    switch (coords[1])
                    {
                        case 0:
                            {
                                return new StatePlaceBR();
                                break;
                            }
                        case 1:
                            {
                                return new StatePlaceR();
                                break;
                            }
                        case 2:
                            {
                                return new StatePlaceTR();
                                break;
                            }
                        default:
                            return new StateTerminal();
                    }
                    break;
                }
            default:
                return new StateTerminal();
        }  
    }

    /// <summary>
    /// Detect if horizontal line can be completed
    /// </summary>
    /// <param name="board">board layout</param>
    /// <param name="target">cell type filter</param>
    /// <param name="coords">coords in which cell should be filled</param>
    /// <returns></returns>
    bool DetectVLine(Board.CellType[,] board, Board.CellType target, out int[] coords)
    {
        Board.CellType empty = Board.CellType.empty;

        for (int i = 0; i < 3; i++)
        {
            if (board[0, i] == target && board[1, i] == target && board[2, i] == empty)
            {
                coords = new int[2] { 2, i };
                return true;
            }  
            if (board[0, i] == target && board[2, i] == target && board[1, i] == empty)
            {
                coords = new int[2] { 1, i };
                return true;
            }
            if (board[1, i] == target && board[2, i] == target && board[0, i] == empty)
            {
                coords = new int[2] { 0, i };
                return true;
            }
        }
        coords = new int[2];
        return false;
    }

    /// <summary>
    /// Detect if vertical line can be completed
    /// </summary>
    /// <param name="board">board layout</param>
    /// <param name="target">cell type filter</param>
    /// <param name="coords">coords in which cell should be filled</param>
    /// <returns></returns>
    bool DetectHLine(Board.CellType[,] board, Board.CellType target, out int[] coords)
    {
        Board.CellType empty = Board.CellType.empty;

        for (int i = 0; i < 3; i++)
        {
            if (board[i, 0] == target && board[i, 1] == target && board[i, 2] == empty)
            {
                coords = new int[2] { i, 2 };
                return true;
            }
            if (board[i, 0] == target && board[i, 2] == target && board[i, 1] == empty)
            {
                coords = new int[2] { i, 1 };
                return true;
            }
            if (board[i, 1] == target && board[i, 2] == target && board[i, 0] == empty)
            {
                coords = new int[2] { i, 0 };
                return true;
            }
        }
        coords = new int[2];
        return false;
    }

    /// <summary>
    /// Detect if diagonal line can be completed
    /// </summary>
    /// <param name="board">board layout</param>
    /// <param name="target">cell type filter</param>
    /// <param name="coords">coords in which cell should be filled</param>
    /// <returns></returns>
    bool DetectDLine(Board.CellType[,] board, Board.CellType target, out int[] coords)
    {
        Board.CellType empty = Board.CellType.empty;
         
        if (board[0, 0] == target && board[1, 1] == target && board[2, 2] == empty)
        {
            coords = new int[2] { 2, 2 };
            return true;
        }
        if (board[0, 0] == target && board[2, 2] == target && board[1, 1] == empty)
        {
            coords = new int[2] { 1, 1 };
            return true;
        }
        if (board[1, 1] == target && board[2, 2] == target && board[0, 0] == empty)
        {
            coords = new int[2] { 0, 0 };
            return true;
        }


        if (board[0, 2] == target && board[1, 1] == target && board[2, 0] == empty)
        {
            coords = new int[2] { 2, 0 };
            return true;
        }
        if (board[2, 0] == target && board[0, 2] == target && board[1, 1] == empty)
        {
            coords = new int[2] { 1, 1 };
            return true;
        }
        if (board[1, 1] == target && board[2, 0] == target && board[0, 2] == empty)
        {
            coords = new int[2] { 0, 2 };
            return true;
        }

        coords = new int[2];
        return false;
    }

}
