﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ChainTransition : Transition {

    List<Dictionary<Board.CellType[,], ChainTransition>> transitionsChain;
    bool terminal;

    #region constructors

    /// <summary>
    /// Terminal transfer constructor
    /// </summary>
    /// <param name="state"></param>
    public ChainTransition() : base(new StateTerminal())
    {
        terminal = true;
    }

    /// <summary>
    /// Singlestate transition
    /// </summary>
    /// <param name="state"></param>
    public ChainTransition(Board.CellType[,] board, State state, ChainTransition chain) : base(state)
    {
        terminal = false;
        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>> (1);
        this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) {{ board, chain }});
    }

    /// <summary>
    /// Manually weighted multistate transition
    /// </summary>
    /// <param name="states"></param>
    /// <param name="weights"></param>
    public ChainTransition(Board.CellType[][,] boards, State[] states, float[] weights, ChainTransition[] chains) : base(states, weights)
    {
        terminal = false;
        if (boards.Length != states.Length || states.Length != chains.Length)
            throw new UnityEngine.UnityException("each state must have it's transition");
        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) {{ boards[i], chains[i] }});
    }

    /// <summary>
    /// Auto equally weighted multistate transition
    /// </summary>
    /// <param name="states"></param>
    public ChainTransition(Board.CellType[][,] boards, State[] states, ChainTransition[] chains) : base(states)
    {
        terminal = false;
        if (boards.Length != states.Length || states.Length != chains.Length)
            throw new UnityEngine.UnityException("each state must have it's transition");
        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { boards[i], chains[i] } });
    }

    #endregion

    #region Chained Constructors

    /// <summary>
    /// Multiple board states leading to multiple actions, relation 1:1
    /// </summary>
    /// <param name="boards"></param>
    /// <param name="chains"></param>
    public ChainTransition(Board.CellType[][,] boards, KeyValuePair<State, ChainTransition>[] chains) : base(new StateTerminal())
    {
        List<State> states = new List<State>();
        List<ChainTransition> transitions = new List<ChainTransition>();
        for (int i = 0; i < chains.Length; i++)
        {
            states.Add(chains[i].Key);
            transitions.Add(chains[i].Value);
        }

        terminal = false;

        if (boards.Length != states.Count || states.Count != transitions.Count)
            throw new UnityEngine.UnityException("each state must have it's transition");
        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { boards[i], transitions[i] } });

        base.states = new List<State>();
        base.weights = new List<float>();
        base.states.AddRange(states);
        for (int i = 0; i < base.states.Count; i++)
            base.weights.Add(1f);
    }


    /// <summary>
    /// Multiple board states leading to one action
    /// </summary>
    /// <param name="boards"></param>
    /// <param name="chain"></param>
    public ChainTransition(Board.CellType[][,] boards, KeyValuePair<State, ChainTransition> chain) : base(new StateTerminal())
    {
        State state;
        ChainTransition transition;
        state = chain.Key;
        transition = chain.Value;

        terminal = false;

        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>(1);
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { boards[i], transition } });

        base.states = new List<State>(1);
        base.states.Add(state);
    }

    /// <summary>
    /// Multiple board states leading to multiple actions, relation n:1
    /// </summary>
    /// <param name="boards"></param>
    /// <param name="chain"></param>
    public ChainTransition(Board.CellType[][][,] boards, KeyValuePair<State, ChainTransition>[] chains) : base(new StateTerminal())
    {
        List<State> states = new List<State>();
        List<ChainTransition> transitions = new List<ChainTransition>();
        for (int i = 0; i < chains.Length; i++)
        {
            states.Add(chains[i].Key);
            transitions.Add(chains[i].Value);
        }

        terminal = false;

        if (boards.Length != states.Count || states.Count != transitions.Count)
            throw new UnityEngine.UnityException("each state must have it's transition");
        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
        {
            Dictionary<Board.CellType[,], ChainTransition> subChain = new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer());
            for (int k = 0; k < boards[i].Length; k++)
                subChain.Add(boards[i][k], transitions[i]);
            this.transitionsChain.Add(subChain);
        }

        base.states = new List<State>();
        base.weights = new List<float>();
        base.states.AddRange(states);
        for (int i = 0; i < base.states.Count; i++)
            base.weights.Add(1f);
    }

    /// <summary>
    /// Single board state leading to one action
    /// </summary>
    /// <param name="board"></param>
    /// <param name="chain"></param>
    public ChainTransition(Board.CellType[,] board, KeyValuePair<State, ChainTransition> chain) : base(new StateTerminal())
    {
        State state;
        ChainTransition transition;
        state = chain.Key;
        transition = chain.Value;

        terminal = false;

        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>(1);
        this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { board, transition } });

        base.states = new List<State>(1);
        base.states.Add(state);
    }

    #endregion

    #region Chained Tree Constructors

    /// <summary>
    /// Chained Singlestate Tree Constructor
    /// </summary>
    /// <param name="board"></param>
    /// <param name="chain"></param>
    public ChainTransition(Dictionary<Board.CellType[,], KeyValuePair<State, ChainTransition>> branches) : base(new StateTerminal())
    {
        Board.CellType[][,] boards = new Board.CellType[branches.Count][,];
        List<State> states = new List<State>();
        List<ChainTransition> transitions = new List<ChainTransition>();

        for (int i = 0; i < branches.Count; i++)
        {
            boards[i] = branches.ElementAt(i).Key;
            states.Add(branches.ElementAt(i).Value.Key);
            transitions.Add(branches.ElementAt(i).Value.Value);
        }

        terminal = false;

        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { boards[i], transitions[i] } });

        base.states = new List<State>();
        base.weights = new List<float>();
        base.states.AddRange(states);
        for (int i = 0; i < base.states.Count; i++)
            base.weights.Add(1f);
    }

    /// <summary>
    /// Chained Singlestate Tree Constructor
    /// </summary>
    /// <param name="board"></param>
    /// <param name="chain"></param>
    public ChainTransition(Dictionary<Board.CellType[,], KeyValuePair<State, ChainTransition>[]> branches) : base(new StateTerminal())
    {
        Board.CellType[][,] boards = new Board.CellType[branches.Count][,];
        List<State> states = new List<State>();
        List<ChainTransition> transitions = new List<ChainTransition>();

        for (int i = 0; i < branches.Count; i++)
        {
            boards[i] = branches.ElementAt(i).Key;
            int k = UnityEngine.Random.Range(0, branches.ElementAt(i).Value.Length); //EACH TIME CHOOSE SOME RANDOM SUBTREE
            states.Add(branches.ElementAt(i).Value[k].Key);
            transitions.Add(branches.ElementAt(i).Value[k].Value);
        }

        terminal = false;

        this.transitionsChain = new List<Dictionary<Board.CellType[,], ChainTransition>>();
        for (int i = 0; i < boards.Length; i++)
            this.transitionsChain.Add(new Dictionary<Board.CellType[,], ChainTransition>(new BoardStateComparer()) { { boards[i], transitions[i] } });

        base.states = new List<State>();
        base.weights = new List<float>();
        base.states.AddRange(states);
        for (int i = 0; i < base.states.Count; i++)
            base.weights.Add(1f);
    }

    #endregion

    /// <summary>
    /// Transfer to the next State from the list of possible states
    /// </summary>
    /// <returns></returns>
    public new KeyValuePair<State,ChainTransition> Transfer(Board.CellType[,] board)
    {
        if (terminal)
            return new KeyValuePair<State, ChainTransition>(states[0], null);
        if (states.Count == 1) // single state transfer
        {
            KeyValuePair < State, ChainTransition > debug = new KeyValuePair<State, ChainTransition>(states[0], transitionsChain.Where(t => t.ContainsKey(board))
                .Select(d => d[board])
                .Single<ChainTransition>());
            return new KeyValuePair<State, ChainTransition>(states[0], transitionsChain.Where(t => t.ContainsKey(board))
                .Select(d => d[board])
                .Single<ChainTransition>());
        }
        else
        { // multi state transfer

            float totalWeights = 0f;
            List<KeyValuePair<State, ChainTransition>> candidates = new List<KeyValuePair<State, ChainTransition>>();
            for (int i = 0; i < states.Count; i++)
            {
                if (transitionsChain[i].ContainsKey(board))
                {
                    candidates.Add(new KeyValuePair<State, ChainTransition>(states[i], transitionsChain[i][board]));
                    totalWeights += weights[i];
                }
            }
            float pointer = UnityEngine.Random.Range(0f, totalWeights);
            for (int i = 0; i < weights.Count; i++)
            {
                pointer -= weights[i];
                if (pointer <= 0f)
                    return candidates[i];
            }
            throw new UnityEngine.UnityException("no proper state were reached");
        }
    }
}
