﻿public static class PossibleBoardStates {

    static Board.CellType x = Board.CellType.x;
    static Board.CellType o = Board.CellType.o;
    static Board.CellType w = Board.CellType.empty;

    /// <summary>
    /// A - old board state
    /// B - new old state
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <returns>Difference between states</returns>
    public static Board.CellType[,] Diff(Board.CellType[,] A, Board.CellType[,] B)
    {
        if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1))
            throw new UnityEngine.UnityException("A and B must be the same size");

        Board.CellType[,] C = new Board.CellType[A.GetLength(0), A.GetLength(1)];

        for (int i = 0; i < A.GetLength(0); i++)
            for (int k = 0; k < A.GetLength(0); k++)
                C[i, k] = A[i, k] == B[i, k] ? Board.CellType.empty : B[i, k] == Board.CellType.o ? Board.CellType.empty : Board.CellType.x;
        return C;
    }

    #region literal states

    public static Board.CellType[,] StateClear = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, w, w },
            { w, w, w }
    };

    public static Board.CellType[,] StateBL = new Board.CellType[3, 3]
    {
            { x, w, w },
            { w, w, w },
            { w, w, w }
    };
    public static Board.CellType[,] StateB = new Board.CellType[3, 3]
    {
            { w, w, w },
            { x, w, w },
            { w, w, w }
    };
    public static Board.CellType[,] StateBR = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, w, w },
            { x, w, w }
    };

    public static Board.CellType[,] StateL = new Board.CellType[3, 3]
{
            { w, x, w },
            { w, w, w },
            { w, w, w }
};
    public static Board.CellType[,] StateC = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, x, w },
            { w, w, w }
    };
    public static Board.CellType[,] StateR = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, w, w },
            { w, x, w }
    };

    public static Board.CellType[,] StateTL = new Board.CellType[3, 3]
{
            { w, w, x },
            { w, w, w },
            { w, w, w }
};
    public static Board.CellType[,] StateT = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, w, x },
            { w, w, w }
    };
    public static Board.CellType[,] StateTR = new Board.CellType[3, 3]
    {
            { w, w, w },
            { w, w, w },
            { w, w, x }
    };

    #endregion


    #region dumb states
    public static Board.CellType[,] State0100 = new Board.CellType[3, 3]
    {
        { x, w, w },
        { w, w, w },
        { w, w, w }
    };

    public static Board.CellType[,] State0101 = new Board.CellType[3, 3]
    {
        { w, w, x },
        { w, w, w },
        { w, w, w }
    };

    public static Board.CellType[,] State0102 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, w, w },
        { w, w, x }
    };

    public static Board.CellType[,] State0103 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, w, w },
        { x, w, w }
    };

    public static Board.CellType[,] State0104 = new Board.CellType[3, 3]
{
        { w, x, w },
        { w, w, w },
        { w, w, w }
};

    public static Board.CellType[,] State0105 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, w, x },
        { w, w, w }
    };

    public static Board.CellType[,] State0106 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, w, w },
        { w, x, w }
    };

    public static Board.CellType[,] State0107 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { x, w, w },
        { w, w, w }
    };

    public static Board.CellType[,] State0108 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, x, w },
        { w, w, w }
    };



    public static Board.CellType[,] State0200 = new Board.CellType[3, 3]
    {
        { x, x, w },
        { w, o, w },
        { w, w, w }
    };

    public static Board.CellType[,] State0201 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, o, x },
        { w, w, x }
    };

    public static Board.CellType[,] State0202 = new Board.CellType[3, 3]
    {
        { w, w, x },
        { w, o, x },
        { w, w, w }
    };

    public static Board.CellType[,] State0203 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, o, w },
        { x, x, w }
    };

    public static Board.CellType[,] State0204 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { w, o, w },
        { w, x, x }
    };

    public static Board.CellType[,] State0205 = new Board.CellType[3, 3]
    {
        { x, w, w },
        { x, o, w },
        { w, w, w }
    };

    public static Board.CellType[,] State0206 = new Board.CellType[3, 3]
    {
        { w, w, w },
        { x, o, w },
        { x, w, w }
    };


    public static Board.CellType[,] State0207 = new Board.CellType[3, 3]
    {
        { w, x, x },
        { w, o, w },
        { w, w, w }
    };



    public static Board.CellType[,] State0300 = new Board.CellType[3, 3]
    {
        { x, x, o },
        { w, o, w },
        { x, w, w }
    };

    public static Board.CellType[,] State0301 = new Board.CellType[3, 3]
    {
        { w, w, o },
        { w, o, x },
        { x, w, x }
    };

    public static Board.CellType[,] State0302 = new Board.CellType[3, 3]
    {
        { x, w, x },
        { w, o, x },
        { w, w, o }
    };

    public static Board.CellType[,] State0303 = new Board.CellType[3, 3]
    {
        { x, w, w },
        { w, o, w },
        { x, x, o }
    };

    public static Board.CellType[,] State0304 = new Board.CellType[3, 3]
    {
        { w, w, x },
        { w, o, w },
        { o, x, x }
    };

    public static Board.CellType[,] State0305 = new Board.CellType[3, 3]
    {
        { x, w, x },
        { x, o, w },
        { o, w, w }
    };

    public static Board.CellType[,] State0306 = new Board.CellType[3, 3]
    {
        { o, w, w },
        { x, o, w },
        { x, w, x }
    };

    public static Board.CellType[,] State0307 = new Board.CellType[3, 3]
    {
        { o, x, x },
        { w, o, w },
        { w, w, x }
    };

    

    public static Board.CellType[,] State0400 = new Board.CellType[3, 3]
    {
        { x, x, o },
        { o, o, x },
        { x, w, w }
    };

    public static Board.CellType[,] State0401 = new Board.CellType[3, 3]
    {
        { w, x, o },
        { w, o, x },
        { x, o, x }
    };

    public static Board.CellType[,] State0402 = new Board.CellType[3, 3]
    {
        { x, o, x },
        { w, o, x },
        { w, x, o }
    };

    public static Board.CellType[,] State0403 = new Board.CellType[3, 3]
    {
        { x, w, w },
        { o, o, x },
        { x, x, o }
    };

    public static Board.CellType[,] State0404 = new Board.CellType[3, 3]
    {
        { w, w, x },
        { x, o, o },
        { o, x, x }
    };

    public static Board.CellType[,] State0405 = new Board.CellType[3, 3]
    {
        { x, o, x },
        { x, o, w },
        { o, x, w }
    };

    public static Board.CellType[,] State0406 = new Board.CellType[3, 3]
    {
        { o, x, w },
        { x, o, w },
        { x, o, x }
    };

    public static Board.CellType[,] State0407 = new Board.CellType[3, 3]
    {
        { o, x, x },
        { x, o, o },
        { w, w, x }
    };
    #endregion



}
