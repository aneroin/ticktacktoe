﻿public class StatePlaceL : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place L");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 0, 1 }));
    }
}