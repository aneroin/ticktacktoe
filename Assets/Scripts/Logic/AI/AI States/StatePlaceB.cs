﻿public class StatePlaceB : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place B");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 1, 0 }));
    }
}