﻿public class StatePlaceT : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place T");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 1, 2 }));
    }
}