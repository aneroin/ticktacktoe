﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardStateComparer : IEqualityComparer<Board.CellType[,]>
{
   public bool Equals(Board.CellType[,] some, Board.CellType[,] other)
    {
        if (some.GetLength(0) != other.GetLength(0) || some.GetLength(1) != other.GetLength(1))
            return false;
        for (int i = 0; i < some.GetLength(0); i++)
            for (int k = 0; k < some.GetLength(0); k++)
                if (some[i, k] != other[i, k])
                    return false;
        return true;
    }

    public int GetHashCode(Board.CellType[,] some)
    {
        int hash = 17;
        for (int i = 0; i < some.GetLength(0); i++)
            for (int k = 0; k < some.GetLength(0); k++)
                hash = hash * 31 + some[i,k].GetHashCode();
        return hash;
    }
}
