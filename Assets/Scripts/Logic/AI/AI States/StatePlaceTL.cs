﻿public class StatePlaceTL : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place TL");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 0, 2 }));
    }
}