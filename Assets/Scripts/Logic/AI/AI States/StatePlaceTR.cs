﻿public class StatePlaceTR : State {
    public void Execute()
    {
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 2, 2 }));
    }
}
