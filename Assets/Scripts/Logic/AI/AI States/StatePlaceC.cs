﻿public class StatePlaceC : State {
    public void Execute()
    {
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 1, 1 }));
    }
}
