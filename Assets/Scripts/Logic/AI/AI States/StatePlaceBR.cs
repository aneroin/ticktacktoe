﻿public class StatePlaceBR : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place BR");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 2, 0 }));
    }
}