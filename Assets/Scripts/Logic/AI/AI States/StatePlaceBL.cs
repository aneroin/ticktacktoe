﻿public class StatePlaceBL : State {
    public void Execute()
    {
        UnityEngine.Debug.Log("Place BL");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 0, 0 }));
    }
}
