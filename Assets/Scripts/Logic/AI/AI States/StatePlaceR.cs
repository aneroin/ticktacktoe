﻿public class StatePlaceR : State
{
    public void Execute()
    {
        UnityEngine.Debug.Log("Place R");
        BoardController board = (BoardController)UnityEngine.Object.FindObjectOfType(typeof(BoardController));
        board.MakeTurn(new DataEvent<int[]>(new int[] { 2, 1 }));
    }
}