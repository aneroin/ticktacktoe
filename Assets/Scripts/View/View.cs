﻿using UnityEngine;

public abstract class View : MonoBehaviour {
    public delegate void ClickEvent(View view);
    protected ClickEvent OnClick;

    public virtual void Bind(ClickEvent callback)
    {
        OnClick += callback;
    }
    public virtual void UnBind(ClickEvent callback)
    {
        OnClick -= callback;
    }
}
