﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuView : View {

    Vector2 BottomLeft;
    Vector2 TopRight;

    GameObject[] menuHolders = new GameObject[3];
    MenuPlatformView[] menuPlatforms = new MenuPlatformView[3];


    public void SetRenderRect(Vector2 bottomLeft, Vector2 topRight)
    {
        Debug.Log("SetRenderRect");

        this.BottomLeft = bottomLeft;
        this.TopRight = topRight;

        ScaleMenu();
    }

    void ScaleMenu()
    {
        Debug.Log("ScaleBoard");

        float width = TopRight.x - BottomLeft.x;
        float height = TopRight.y - BottomLeft.y;

        float h_gap = (width / 3f) * 0.8f;
        float h_shift = h_gap * 2f;

        menuHolders[0] = Instantiate((GameObject) Resources.Load("Prefs/Menu"));
        menuHolders[0].transform.parent = transform;
        menuHolders[0].transform.position = new Vector3(1f * h_gap - h_shift, -1f, 1);
        menuPlatforms[0] = menuHolders[0].AddComponent<MenuPlatformView>().Setup(Game.GameState.play, "easy");

        menuHolders[1] = Instantiate((GameObject)Resources.Load("Prefs/Menu"));
        menuHolders[1].transform.parent = transform;
        menuHolders[1].transform.position = new Vector3(2f * h_gap - h_shift, 1f, 0);
        menuPlatforms[1] = menuHolders[1].AddComponent<MenuPlatformView>().Setup(Game.GameState.play, "normal");

        menuHolders[2] = Instantiate((GameObject)Resources.Load("Prefs/Menu"));
        menuHolders[2].transform.parent = transform;
        menuHolders[2].transform.position = new Vector3(3f * h_gap - h_shift, 0f, 1);
        menuPlatforms[2] = menuHolders[2].AddComponent<MenuPlatformView>().Setup(Game.GameState.play, "unfair");

    }

    public void DestroyView()
    {
        for (int k = 0; k < 3; k++) {
            Destroy(menuPlatforms[k]);
            menuHolders[k].AddComponent<Throttle>();
        }
        Destroy(gameObject, 4f);
    }

    public override void Bind(ClickEvent callback)
    {
        base.Bind(callback);
        foreach(View menu in menuPlatforms)
        {
            menu.Bind(callback);
        }
    }
    public override void UnBind(ClickEvent callback)
    {
        base.UnBind(callback);
        foreach (View menu in menuPlatforms)
        {
            menu.UnBind(callback);
        }
    }
}
