﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : View {

    Vector2 BottomLeft;
    Vector2 TopRight;

    float width;
    float height;

    float cell_width;
    float cell_height;

    float gap_width;
    float gap_height;

    public GameObject X;
    public GameObject O;
    public GameObject E;

    public GameObject[,] CellViews;
    public GameObject[,] PlatformViews;

    public void SetRenderRect(Vector2 bottomLeft, Vector2 topRight)
    {
        Debug.Log("SetRenderRect");

        this.BottomLeft = bottomLeft;
        this.TopRight = topRight;

        ScaleBoard();
    }

    void ScaleBoard()
    {
        Debug.Log("ScaleBoard");

        float width = TopRight.x - BottomLeft.x;
        float height = TopRight.y - BottomLeft.y;

        cell_width = (width / 3f) * 0.8f;
        gap_width = (width / 3f) * 0.2f;

        cell_height = (height / 3f) * 0.8f;
        gap_height = (height / 3f) * 0.2f;

        gameObject.transform.position = CalculateOrigin(1, 1);

        for (int k = 0; k < 3; k++)
            for (int i = 0; i < 3; i++)
                if (CellViews[i, k] != null)
                    CellViews[i, k].transform.position = CalculateOrigin(i, k);
    }

    public void DrawPlatforms()
    {
        for (int k = 0; k < 3; k++)
            for (int i = 0; i < 3; i++)
            {
                PlatformViews[i, k] = GameObject.Instantiate(E, CalculateOrigin(i, k) + (Vector3.down * 1), Quaternion.identity, transform);
                PlatformViews[i, k].AddComponent<PlatformView>().Setup(i, k).Bind(base.OnClick);
            }
    }

    public void DestroyView()
    {
        for (int k = 0; k < 3; k++)
            for (int i = 0; i < 3; i++)
            {
                PlatformViews[i, k].AddComponent<Throttle>();
                PlatformViews[i, k].GetComponent<PlatformView>().UnBind(base.OnClick);
                Destroy(PlatformViews[i, k], 4f);
            }
    }

    public void SetData(Board.CellType[,] boardData)
    {
        for (int k = 0; k < 3; k++)
            for (int i = 0; i < 3; i++)
                if (boardData[i, k] == Board.CellType.empty)
                {
                    if (CellViews[i, k] != null)
                    {
                        GameObject.Destroy(CellViews[i, k]);
                        PlatformViews[i, k].GetComponent<Sticker>().UnStick();
                    }
                } 
                else if (boardData[i, k] == Board.CellType.x)
                {
                    if (CellViews[i, k] == null) {
                        CellViews[i, k] = GameObject.Instantiate(X, CalculateOrigin(i, k), Quaternion.identity, transform); PlatformViews[i,k].GetComponent<Sticker>().Stick(CellViews[i, k]);}
                    else if (CellViews[i, k] != null && CellViews[i, k].tag!="X" ) {
                        GameObject.Destroy(CellViews[i, k]);
                        CellViews[i, k] = GameObject.Instantiate(X, CalculateOrigin(i, k), Quaternion.identity, transform); PlatformViews[i, k].GetComponent<Sticker>().Stick(CellViews[i, k]);}
                }
                else if (boardData[i, k] == Board.CellType.o)
                {
                    if (CellViews[i, k] == null) {
                        CellViews[i, k] = GameObject.Instantiate(O, CalculateOrigin(i, k), Quaternion.identity, transform); PlatformViews[i, k].GetComponent<Sticker>().Stick(CellViews[i, k]);}
                    else if (CellViews[i, k] != null && CellViews[i, k].tag != "O") {
                        GameObject.Destroy(CellViews[i, k]);
                        CellViews[i, k] = GameObject.Instantiate(O, CalculateOrigin(i, k), Quaternion.identity, transform); PlatformViews[i, k].GetComponent<Sticker>().Stick(CellViews[i, k]);}
                }
    }

    public void Awake()
    {
        X = (GameObject) Resources.Load("Prefs/X");
        O = (GameObject) Resources.Load("Prefs/O");
        E = (GameObject) Resources.Load("Prefs/Empty");
        CellViews = new GameObject[3,3];
        PlatformViews = new GameObject[3,3];
        SetRenderRect(BottomLeft, TopRight);
    }

    public void Start()
    {
        DrawPlatforms();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        for (int k = 0; k < 3; k++)
            for (int i = 0; i < 3; i++)
                Gizmos.DrawSphere(CalculateOrigin(i,k), 1);
    }

    public Vector3 CalculateOrigin(int i, int k)
    {
        return new Vector3(i * cell_width + i * gap_width, 0f, k * cell_height + k * gap_height);
    }

    public Vector2 CalculateCell(Vector2 worldPosition)
    {
        try
        {
            Vector2 shift = worldPosition - BottomLeft;
            Vector2 cell = Vector2.zero;
            cell.x = Mathf.Clamp(Mathf.FloorToInt(shift.x / (cell_width + gap_width)) -1, 0, 2);
            cell.y = Mathf.Clamp(Mathf.FloorToInt(shift.y / (cell_height + gap_height)) -1, 0, 2);
            return cell;
        } catch (System.Exception e)
        {
            throw e;
        }
        
    }
}
