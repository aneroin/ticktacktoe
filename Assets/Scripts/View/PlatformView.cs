﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformView : View {
    
    private int x;
    private int y;

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public PlatformView Setup(int x, int y)
    {
        this.x = x;
        this.y = y;
        gameObject.AddComponent<BoxCollider>();
        return this;
    }

    public void OnMouseDown()
    {
        if (base.OnClick != null)
            base.OnClick(this);
    }

}
