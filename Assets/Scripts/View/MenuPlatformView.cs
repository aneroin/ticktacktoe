﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlatformView : View {

    public Game.GameState state;
    public string payload;

    public Game.GameState getState()
    {
        return this.state;
    }

    public MenuPlatformView Setup(Game.GameState state, string payload)
    {
        this.state = state;
        this.payload = payload;
        gameObject.GetComponentInChildren<TextMesh>().text = payload;
        gameObject.AddComponent<BoxCollider>();
        return this;
    }

    public void OnMouseDown()
    {
        if (base.OnClick != null)
            base.OnClick(this);
    }
}
