﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throttle : MonoBehaviour {

    Vector3 speed;

	// Use this for initialization
	void Start () {
        speed = Random.insideUnitSphere;
        speed.y = Random.Range(4.5f, 7.5f);

    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(speed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(speed.x, 0f, speed.z);
	}
}
