﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyIn : MonoBehaviour
{

    Vector3 speed;
    Vector3 origin;

    // Use this for initialization
    void Start()
    {
        speed = Vector3.zero;
        speed.y = Random.Range(3.5f, 4.5f);
        origin = transform.position;
        transform.position += Vector3.down * 10f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(speed.x, 0f, speed.z);
        if (Vector3.Distance(transform.position, origin) < 0.2f)
        {
            transform.position = origin;
            gameObject.AddComponent<HoverWiggly>();
            Destroy(gameObject.GetComponent<FlyIn>());
        }
    }
}
