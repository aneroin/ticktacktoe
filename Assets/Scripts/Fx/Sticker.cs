﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sticker : MonoBehaviour {

    public GameObject stickPosition;
    GameObject stickedObject;

    public void Stick(GameObject target)
    {
        stickedObject = target;
        stickedObject.transform.parent = stickPosition.transform;
        stickedObject.transform.localPosition = Vector3.zero;
    }

    public void UnStick()
    {
        stickedObject.transform.parent = null;
        stickedObject = null;
    }

}
