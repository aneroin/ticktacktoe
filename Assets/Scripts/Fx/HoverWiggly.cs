﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverWiggly : MonoBehaviour {

    Vector3 poi;

    Vector3 wiggleComponent;
    float t;
    Random r;

    public float wiggleScale = 0.5f;

	// Use this for initialization
	void Start () {
        poi = transform.position + (Vector3.up * 5f);
        wiggleComponent = new Vector3(Random.Range(3.2f, 4.6f), Random.Range(1.7f, 2.9f), Random.Range(3.2f, 4.6f));
        t = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime;
        transform.Translate(new Vector3(Mathf.Sin(t * wiggleComponent.x), Mathf.Sin(t * wiggleComponent.y), Mathf.Sin(t * wiggleComponent.z)) * Time.deltaTime * wiggleScale);

        Vector3 difference = poi - transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg -90f;
        float rotationX = Mathf.Atan2(difference.y, difference.z) * Mathf.Rad2Deg - 90f;
        transform.rotation = Quaternion.Euler(-rotationX, 0.0f, rotationZ);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(poi, 0.25f);
    }
}
