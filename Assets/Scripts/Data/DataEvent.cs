﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEvent<T> {

    T payload;

    public DataEvent (T payload)
    {
        this.payload = payload;
    }

    public T Data()
    {
        return payload;
    }

}
