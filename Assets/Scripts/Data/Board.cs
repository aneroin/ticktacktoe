﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board {

    Controller.EventCallback<CellType[,]> eventCallback;

    private CellType[,] board;

    public Board()
    {
        this.board = new CellType[3, 3];
        for (int i = 0; i < 3; i++)
            for (int k = 0; k < 3; k++)
                board[i, k] = CellType.empty;
    }

    public void Bind(Controller.EventCallback<CellType[,]> eventCallback)
    {
        this.eventCallback += eventCallback;
    }

    public void Unbind(Controller.EventCallback<CellType[,]> eventCallback)
    {
        this.eventCallback -= eventCallback;
    }

    public CellType[,] GetBoard()
    {
        return board;
    }

    public void SetBoard(CellType[,] board)
    {
        this.board = board;
        if (this.eventCallback != null)
        {
            eventCallback(new DataEvent<CellType[,]>(board));
        }
    }

    public CellType GetCell(int i, int k)
    {
        return board[i, k];
    }

    public void SetCell(int i, int k, CellType type)
    {
        board[i, k] = type;
        if (this.eventCallback != null)
        {
            eventCallback(new DataEvent<CellType[,]>(board));
        }
    }



    public enum CellType
    {
        empty,
        o,
        x
    }
}
