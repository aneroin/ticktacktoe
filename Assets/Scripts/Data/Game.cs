﻿using System.Collections;
using System.Collections.Generic;

public class Game {

    public enum GameState
    {
        none,
        menu,
        introduction,
        play,
        playerA,
        playerB,
        draw,
        score
    }

    Controller.EventCallback<int[]> eventCallback;
    Controller.EventCallback<GameState> stateCallback;

    GameState _state;
    public GameState state {
        get { return _state; }
        set {
            GameState _prev_state = _state;
            _state = value;
            stateCallback(new DataEvent<GameState>(_state));
            /*
            if (_prev_state != _state && stateCallback != null)
                stateCallback(new DataEvent<GameState>(_state));
                */
        }
    }

    int _matches;
    int _playerA_wins;
    int _playerB_wins;
    int _draws;

    public Game()
    {
        _state = GameState.none;
    }

    public void Bind(Controller.EventCallback<int[]> eventCallback)
    {
        this.eventCallback += eventCallback;
    }

    public void Unbind(Controller.EventCallback<int[]> eventCallback)
    {
        this.eventCallback -= eventCallback;
    }

    public void Bind(Controller.EventCallback<GameState> stateCallback)
    {
        this.stateCallback += stateCallback;
    }

    public void Unbind(Controller.EventCallback<GameState> stateCallback)
    {
        this.stateCallback -= stateCallback;
    }

    public void PlayerAWin()
    {
        _matches++;
        _playerA_wins++;
        if (eventCallback != null)
            eventCallback(new DataEvent<int[]>(new int[] { _playerA_wins, _playerB_wins, _draws }));
        state = GameState.playerA;
    }

    public void PlayerBWin()
    {
        _matches++;
        _playerB_wins++;
        if (eventCallback != null)
            eventCallback(new DataEvent<int[]>(new int[] { _playerA_wins, _playerB_wins, _draws }));
        state = GameState.playerB;
    }

    public void Draw()
    {
        _matches++;
        _draws++;
        if (eventCallback != null)
            eventCallback(new DataEvent<int[]>(new int[] { _playerA_wins, _playerB_wins, _draws }));
        state = GameState.draw;
    }

    public void Menu()
    {
        state = GameState.menu;
    }

    public GameState GetGameState()
    {
        return state;
    }

    public int GetPlayerAWins()
    {
        return _playerA_wins;
    }

    public int GetPlayerBWins()
    {
        return _playerB_wins;
    }

    public int GetDraws()
    {
        return _draws;
    }

    public int GetTotalMatches()
    {
        return _matches;
    }
	
}
